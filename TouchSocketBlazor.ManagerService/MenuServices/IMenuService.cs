﻿using BootstrapBlazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TouchSocketBlazor.ManagerService
{
    public interface IMenuService
    {
        /// <summary>
        /// 获取菜单列表
        /// </summary>
        /// <returns></returns>
        List<MenuItem>? GetMenuItems();
    }
}
