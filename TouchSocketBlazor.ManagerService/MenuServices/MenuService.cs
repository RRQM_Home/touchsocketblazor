﻿using BootstrapBlazor.Components;
using Microsoft.AspNetCore.Components.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TouchSocketBlazor.ManagerService
{
    public class MenuService : IMenuService
    {
        List<MenuItem>? IMenuService.GetMenuItems()
        {
            var menus = new List<MenuItem>
        {
            new MenuItem() { Text = "目录一", Icon = "fa-solid fa-fw fa-home"},
            new MenuItem() { Text = "目录二", Icon = "fa-solid fa-fw fa-flag", Url = "/" , Match = NavLinkMatch.All},
            new MenuItem() { Text = "目录三", Icon = "fa-solid fa-fw fa-check-square", Url = "/counter" },
            new MenuItem() { Text = "目录四", Icon = "fa-solid fa-fw fa-database", Url = "fetchdata" },
            new MenuItem() { Text = "目录五", Icon = "fa-solid fa-fw fa-table", Url = "table" },
            new MenuItem() { Text = "TcpServiceManage", Icon = "fa-solid fa-fw fa-users", Url = "/TcpServiceManage" }
        };

            return menus;
        }
    }
}
