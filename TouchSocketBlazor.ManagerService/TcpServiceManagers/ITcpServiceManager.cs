﻿using BootstrapBlazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TouchSocketBlazor.Models.Tcp;

namespace TouchSocketBlazor.ManagerService.TcpServiceManagers
{
    public interface ITcpServiceManager: IDataService<TcpServiceModel>
    {
    }
}
