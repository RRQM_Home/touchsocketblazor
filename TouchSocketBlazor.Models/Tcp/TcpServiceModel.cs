﻿using BootstrapBlazor.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Xml.Linq;
using TouchSocket.Sockets;

namespace TouchSocketBlazor.Models.Tcp
{
    public class TcpServiceModel
    {
        [Display(Name = "主键")]
        public string ID { get; set; }

        [Required(ErrorMessage = "{0}不能为空")]
        [Display(Name = "名称")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0}不能为空")]
        [Display(Name = "监听地址组")]
        public string IpHosts { get; set; }

        [Required(ErrorMessage = "{0}不能为空")]
        [Display(Name = "状态")]
        public ServerState ServerState { get; set; } = ServerState.Running;
    }
}
