﻿using Microsoft.AspNetCore.Authentication.Cookies;
using System.Text;
using TouchSocketBlazor.ManagerService;
using TouchSocketBlazor.ManagerService.TcpServiceManagers;
using TouchSocketBlazor.Shared.Data;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

builder.Services.AddControllersWithViews();
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie();
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();

builder.Services.AddBootstrapBlazor();

//添加菜单服务
builder.Services.AddSingleton<IMenuService, MenuService>();
//添加tcp管理服务
builder.Services.AddSingleton<ITcpServiceManager, TcpServiceManager>();

// 增加 Table 数据服务操作类
builder.Services.AddTableDemoDataService();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Error");
}

app.UseStaticFiles();

app.UseRouting();
app.UseAuthentication();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");
app.MapDefaultControllerRoute();

app.Run();
