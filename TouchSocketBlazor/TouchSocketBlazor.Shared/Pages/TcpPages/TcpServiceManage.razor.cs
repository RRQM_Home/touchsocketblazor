﻿using BootstrapBlazor.Components;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TouchSocketBlazor.ManagerService.TcpServiceManagers;
using TouchSocketBlazor.Models.Tcp;
using TouchSocketBlazor.Shared.Data;
using TouchSocketBlazor.Shared.Pages.Dialogs;

namespace TouchSocketBlazor.Shared.Pages.TcpPages
{
    public partial class TcpServiceManage
    {
        [Inject]
        [NotNull]
        public ITcpServiceManager? TcpServiceManager { get; set; }

        [Inject]
        [NotNull]
        public ToastService? ToastService { get; set; }

        [Inject]
        [NotNull]
        private DialogService? DialogService { get; set; }

        private Table<TcpServiceModel>? m_table;

        /// <summary> 
        /// 
        /// </summary>
        private static IEnumerable<int> PageItemsSource => new int[] { 20, 40 };

        private async Task OnEditClickCallback(IEnumerable<TcpServiceModel> models)
        {
            var model = models.ToArray()[0];
            if (model.ServerState == TouchSocket.Sockets.ServerState.Running)
            {
                await ToastService.Warning("不允许操作", "服务器在运行状态时不允许编辑！！");
                return;
            }

            var op = new DialogOption()
            {
                Title = "数据查询窗口",
                ShowFooter = false,
                BodyContext = model,
            };
            op.Component = BootstrapDynamicComponent.CreateComponent<TcpServiceDialog>();
            await DialogService.Show(op);
        }

        private async Task OnStopClickCallback(IEnumerable<TcpServiceModel> models)
        {
            var model = models.ToArray()[0];
            model.ServerState = TouchSocket.Sockets.ServerState.Stopped;
            await this.m_table!.QueryAsync();
        }

        private void SelectedRowsChanged(List<TcpServiceModel> models)
        {

        }
    }
}
